<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	<!-- FOOTER SECTION ENDS -->
	<footer>
		<div class='footer_holder'>
			<div>
				<ul>
					<li class="top_footer_li">DOMAINS</li>
					<li>Register Domain Name</li>
					<li>View Domain Pricing</li>
					<li>Bulk Domain Register</li>
					<li>Bulk Domain Transfer</li>
					<li>Whois Lookup</li>
					<li>Name Suggestion Tool</li>
					<li>Free with Every Domain</li>
					<li>View Promos</li>
				</ul>
			</div>
			<div>
				<ul>
					<li class="top_footer_li">SUPPORT</li>
					<li>View Knowledge Base</li>
					<li>Contract Support</li>
					<li>Report Abuse</li>
				</ul>
			</div>
			<div>
				<ul>
					<li class="top_footer_li">RESELLERS</li>
					<li>Join Reseller Program</li>
				</ul>
			</div>
			
		</div>
		<div class="footer_below">
			<div class="fb_first">
				<p>Copyright Domain Pal All rights reserved</p>
			</div>
			<div class="fb_second">
				<a href="">Privacy Policy</a>
				<a href="">Legal Agreement</a>
			</div>
		</div>
	</footer>
	<!-- FOOTER SECTION ENDS -->
	

<?php wp_footer(); ?>

</body>
</html>
