<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: Registration Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>


<!-- HERO SECTION -->
<section class='hero registration_hero' style="background-image:url(<?php the_field('banner'); ?>)">
	<div class="overlay">
		<div class='text_box'>
			<h1><?php the_field('hero_title'); ?></h1>
			<a href="" class="button">DOMAIN</a><br/>
			<input type="text">
		</div>
	</div>
</section>
<!-- HERO SECTION END -->
<!-- DOMAINS SECTION -->
<section class="moreDomains registration_domains">
	<p>Most Popular Domain Extensions</p>
	<div class="wrapper">
		<div class="inner inner-first"><p>.biz</p></div>
		<div class="inner"><p>.info</p></div>
		<div class="inner"><p>.com</p></div>
		<div class="inner"><p>.org</p></div>
		<div class="inner"><p>.us</p></div>
		<div class="inner"><p>.name</p></div><br/>
	</div><br/>
	<div class="wrapper">
		<div class="inner inner-first"><p>.in</p></div>
		<div class="inner"><p>.co.in</p></div>
		<div class="inner"><p>.net.in</p></div>
		<div class="inner"><p>.org.in</p></div>
		<div class="inner"><p>.net</p></div>
		<div class="inner"><p>.co</p></div><br/>
	</div>
</section>
<!-- DOMAINS SECTION END -->

	<!-- SLIDER SECTION -->
	<section class="sliderSection">
		<h1 id="slide-h1">GET OVER $100 WORTH OF FREE <br/>SERVICES WITH EVERY DOMAIN YOU REGISTER</h1>
		<div class="multiple-items">
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/bulk_tools_icon.png" alt="">
				<h1>BULK TOOLS</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/dns_icon.png" alt="">
				<h1>DNS MANAGEMENT</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/lock_icon.png" alt="">
				<h1>DNS MANAGEMENT</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/bulk_tools_icon.png" alt="">
				<h1>BULK TOOLS</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/dns_icon.png" alt="">
				<h1>DNS MANAGEMENT</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/lock_icon.png" alt="">
				<h1>DNS MANAGEMENT</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
		</div>
	</section>
	<!-- SLIDER SECTION END -->

<script type="text/javascript">
	$(document).ready(function(){
		$('.multiple-items').slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: true,
			centerMode: true
		});
	});
</script>
<?php
get_footer();
