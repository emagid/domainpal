<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    
<!--    CUSTOM STYLE-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/urock.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/langdon/font.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">
    
    
<!--    CUSTOM SCRIPT-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- NAVIGATION BAR MENU -->
	<header>
        <div class="above_header">
            <a href="">Login/Signup</a>
            <a href="">English</a>
            <a href="">USD</a>
            <img src="wp-content/uploads/2018/03/cart_icon.png" alt="">
        </div>
        <a href="/" class="logo"><img src="wp-content/uploads/2018/03/logo.png" alt=""></a>
		<nav>
            <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1'
                    ) );
                ?>
		</nav>

		<!-- Mobile -->
		<div id="hamburger">
            <div class="bar1 hamburger_line"></div>
            <div class="bar2 hamburger_line"></div>
            <div class="bar3 hamburger_line"></div>
        </div>

        <div class='dropdown'>
            <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1'
                    ) );
                ?>
        </div>
        <!-- Mobile Ends -->
	</header>
	<!-- NAVBAR ENDS -->


