<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: TLDS Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero tlds_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('hero_title'); ?></h1>
				<p><?php the_field('hero_subtitle'); ?></p>
				<input type="text" placeholder="Domain Name"><br/>
            </div>
        </div>
	</section>
	<!-- HERO SECTION END -->
	<!-- CONTENT SECTION -->
	<section class="tlds_content">
		<div class="wrapper">
			<h1>Why are new domain extensions being introduced?</h1>
			<p>Today, it's hard for busineses and individuals to find domains that are both relevant <br/>and
				available. With over a hundred new domain extensions like .guru, .photography and .clothing being introduced, it will be
				much easier to find the right domain <br/>for your website.
			</p>
		</div>
		<div class="wrapper">
			<h1>How does the pre-registration process work?</h1>
			<p>Pre-registration lets you place an order for a domain before its made available to the general public. Once placed, the moment the domain becomes available 
				we attempt to purchase it on your behalf. <br/>In case the domain is not available, your money is refunded.
			</p>
		</div>
		<div class="wrapper">
			<h1>When will the new domain extensions become available?</h1>
			<p>The new extensions will become available for purchase over the course of the next 12-14 months. Use our pre-registration
				service to ensure that you get your domain as soon as it <br/>becomes available.
			</p>
		</div>
	</section>
	<!-- CONTENT SECTION END -->



  

<?php
get_footer();
