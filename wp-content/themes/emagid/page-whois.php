<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: WhoIs Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('hero_title'); ?></h1>
				<p><?php the_field('hero_subtitle'); ?></p>
				<input type="text"><br/>
				<a href="" class="button">START</a>
            </div>
        </div>
	</section>
	<!-- HERO SECTION END -->
	<!-- DOMAINS SECTION -->
	<section class="moreDomains">
		<div class="wrapper">
			<div class="inner inner-first"><p>.BIZ<br/><span>$11.99</span></p></div>
			<div class="inner"><p>.INFO<br/><span>$12.99</span></p></div>
			<div class="inner"><p>.COM<br/><span>$10.99</span></p></div>
			<div class="inner"><p>.ORG<br/><span>$11.99</span></p></div><br/>
			<div class="domainsBtn"><a href="">More domains ></a></div>
		</div>
	</section>
	<!-- DOMAINS SECTION END -->
	<!-- SLIDER SECTION -->
	<section class="sliderSection">
		<h1 id="slide-h1">GET OVER $100 WORTH OF FREE <br/>SERVICES WITH EVERY DOMAIN YOU REGISTER</h1>
		<div class="multiple-items">
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/bulk_tools_icon.png" alt="">
				<h1>BULK TOOLS</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/dns_icon.png" alt="">
				<h1>DNS MANAGEMENT</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/lock_icon.png" alt="">
				<h1>DNS MANAGEMENT</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/bulk_tools_icon.png" alt="">
				<h1>BULK TOOLS</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/dns_icon.png" alt="">
				<h1>DNS MANAGEMENT</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
			<section class='inner-slide'>	
				<img src="/wp-content/uploads/2018/03/lock_icon.png" alt="">
				<h1>DNS MANAGEMENT</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
			</section>
		</div>
	</section>
	<!-- SLIDER SECTION END -->
	<!-- SUPPORT SECTION  -->
	<section class="support" style="background-image:url('/wp-content/uploads/2018/03/support_hero.png')">
        <div class="overlay">
            <div class='text_box'>
                <h1>24 HOUR SUPPORT</h1>
				<a href="" class="button">CONTACT</a>
                
            </div>
        </div>
	</section>
	<!-- SUPPORT SECTION END-->
	<!-- BROWSE DOMAINS SECTION  -->
	<section class="browseDomains">
		<div class="row">
			<img src="wp-content/uploads/2018/03/domains_hero-e1522166483611.png" alt="">
			<div class="para right_element float_me">
				<div class="text_box">
					<h1>ALL THE DOMAINS<br/>YOU'LL EVER NEED</h1><br/>
					<a href="" class="button">BROWSE DOMAINS</a>
				</div>
			</div>
		</div>
	</section>
	<!-- BROWSE DOMAINS SECTION END -->


	
 <script type="text/javascript">
    $(document).ready(function(){
		$('.multiple-items').slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: true,
			centerMode: true
		});
    });
  </script>
  

<?php
get_footer();
