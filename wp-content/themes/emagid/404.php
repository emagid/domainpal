<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package emagid
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error_page" style="text-align: center;">
				<div class="">
					<h1 style="font-size: 250px; line-height: 1em;">404</h1><br/>
					<h1 class="tagline" style="font-size: 50px;"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'emagid' ); ?></h1>
				</div>

				<div class="page-content">
					<p style="margin-top: 25px;">It looks like nothing was found at this location.</p><br/>
					<a href="/"><button style="margin-top: 25px; margin-bottom: 25px;" class="button">HOME</button></a>
					
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
